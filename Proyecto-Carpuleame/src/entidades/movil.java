package entidades;


import java.io.Serializable;

import entidades.Enumerados.estadoMovil;

public class movil implements Serializable {
 
	private static final long serialVersionUID = 1L;
	
	private String Matricula;
	
	private String Nombre;
	
	private estadoMovil estado;
	
    public movil(){
    }
    public movil(String Mat, String Nom){
        setMatricula(Mat);
        setNombre(Nom);
    }

    /**
     * @return the Matricula
     */
    public String getMatricula() {
        return Matricula;
    }

    /**
     * @param Matricula the Matricula to set
     */
    public void setMatricula(String Matricula) {
        this.Matricula = Matricula;
    }

    /**
     * @return the Nombre
     */
    public String getNombre() {
        return Nombre;
    }

    /**
     * @param Nombre the Nombre to set
     */
    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }
	public estadoMovil getEstado() {
		return estado;
	}
	public void setEstado(estadoMovil estado) {
		this.estado = estado;
	}
    
    
}
