package entidades;

import entidades.Enumerados.estadoMovil;

public class NodoArbol {
	
    private int documento;
    private String matricula;
    private estadoMovil estado;
    private NodoArbol hijoIzquierdo;
    private NodoArbol hijoDerecho;
    
    
    
    public NodoArbol(int doc, String mat){
        
        this.documento = doc;
        this.matricula = mat;
        this.estado = estadoMovil.DESHABILITADO;
        this.hijoDerecho = null;
        this.hijoIzquierdo = null;
        
    }
    
    @Override
	public String toString(){
    return getMatricula() + " su conductor es: " + getDocumento();
    }

    /**
     * @return the estado
     */
    public estadoMovil getEstado() {
        return estado;
    }

    /**
     * @param estado the estado to set
     */
    public void setEstado(estadoMovil estado) {
        this.estado = estado;
    }

	public NodoArbol getHijoIzquierdo() {
		return hijoIzquierdo;
	}

	public void setHijoIzquierdo(NodoArbol hijoIzquierdo) {
		this.hijoIzquierdo = hijoIzquierdo;
	}

	public NodoArbol getHijoDerecho() {
		return hijoDerecho;
	}

	public void setHijoDerecho(NodoArbol hijoDerecho) {
		this.hijoDerecho = hijoDerecho;
	}

	public int getDocumento() {
		return documento;
	}

	public void setDocumento(int documento) {
		this.documento = documento;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
    

    
    
    
}
