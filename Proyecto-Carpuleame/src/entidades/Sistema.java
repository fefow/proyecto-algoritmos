package entidades;

import interfaces.ISistema;

public class Sistema implements ISistema {

    private ABB Arvol;
    private static Sistema instance;
    
    //Singleton
    public static Sistema getInstance(){
    	if(instance == null){
    		instance = new Sistema();
    	}
    	return instance;
    }
    
    //Constructor
    public Sistema() {
    	Arvol = new ABB();
    }

    @Override
	public Retorno inicializarSistema(int cantPuntos) {
        return new Retorno();
    }

    @Override
	public Retorno destruirSistema() {
        return new Retorno();
    }

    @Override
    public Retorno registrarMovil(int documento, String matricula) {

        
        this.Arvol.agregarNodo(documento, matricula);
        if (!Arvol.esVacio()) {
            Arvol.inOrden(Arvol.raiz);
        } else {
            System.out.println("Vacio");
        }
        
        return new Retorno(Retorno.Resultado.OK);
    }

    @Override
    public Retorno deshabilitarMovil(String matricula) {

        return new Retorno();
    }

    @Override
    public Retorno eliminarMovil(String matricula) {
  
        return new Retorno();
    }

    @Override
    public Retorno habilitarMovil(String matricula) {

        return new Retorno();
    }

    @Override
    public Retorno asignarUbicacionMovil(String matricula, Double coordX, Double coordY) {

        return new Retorno();
    }

    @Override
    public Retorno buscarMovil(String matricula) {

        return new Retorno();
    }

    @Override
    public Retorno informeMoviles() {

        return new Retorno();
    }

    @Override
    public Retorno registrarEsquina(Double coordX, Double coordY) {
    	
    	Nodo NodoEsquina;
        
    	Esquina esq = new Esquina(coordX, coordY);
        
        NodoEsquina = new Nodo(esq);

            //Nodo aux=NodoEsquina;
        //for(int i=0; i<2 ;i++){
        //    Esquina aux2=(Esquina)aux.getContiene();
        //
//            if 
        return new Retorno(Retorno.Resultado.OK);
    }

    @Override
    public Retorno registrarTramo(Double coordXi, Double coordYi, Double coordXf, Double coordYf, int metros) {

        return new Retorno();
    }

    @Override
    public Retorno eliminarEsquina(Double coordX, Double coordY) {

        return new Retorno();
    }

    @Override
    public Retorno eliminarTramo(Double coordXi, Double coordYi, Double coordXf, Double coordYf) {

        return new Retorno();
    }

    @Override
    public Retorno movilMasCercano(Double coordX, Double coordY) {

        return new Retorno();
    }

    @Override
    public Retorno verMovilesEnRadio(Double coordX, Double coordY, int radio) {

        return new Retorno();
    }

    @Override
    public Retorno verMapa() {

        return new Retorno();
    }

}
