package entidades;

public class Esquina {
    
	private Double coordX;
    
    private Double coordY;
  
      public Esquina(double coordX, double coordY) {
        this.setCoordX(coordX);
        this.setCoordY(coordY);
    }

	public Double getCoordX() {
		return coordX;
	}

	public void setCoordX(Double coordX) {
		this.coordX = coordX;
	}

	public Double getCoordY() {
		return coordY;
	}

	public void setCoordY(Double coordY) {
		this.coordY = coordY;
	}
      
    
      
}
