package entidades;

public class ABB {

    NodoArbol raiz;

    public ABB() {
        raiz = null;

    }

    //Insertar nodo
    public Retorno agregarNodo(int doc, String mat) {

        NodoArbol nuevo = new NodoArbol(doc, mat);
        if (raiz == null) {
            raiz = nuevo;

        } else {

            NodoArbol aux = raiz;
            NodoArbol padre;
            while (true) {
                padre = aux;
                if (doc < aux.getDocumento()) {
                    aux = aux.getHijoIzquierdo();
                    if (aux == null) {
                        padre.setHijoIzquierdo(nuevo);
                        return new Retorno(Retorno.Resultado.OK);
                    }
                } else {
                    aux = aux.getHijoDerecho();
                    if (aux == null) {
                        padre.setHijoDerecho(nuevo);
                        return new Retorno(Retorno.Resultado.OK);
                    }
                }
            }

        }
        return new Retorno(Retorno.Resultado.ERROR_1);
    }

    public boolean esVacio() {

        return raiz == null;

    }
//REcorrido

    public void inOrden(NodoArbol r) {

        if (r != null) {

            inOrden(r.getHijoIzquierdo());
            System.out.println(r.getDocumento());
            System.out.println(r.getMatricula());

            inOrden(r.getHijoDerecho());

        }
    }

    //Busar
    public NodoArbol buscarNodo(String mat, NodoArbol raiz) {
        System.out.println("Entro por: " + raiz.getMatricula());
        NodoArbol res = null;
        if (raiz.getMatricula() == mat) {
            res = raiz;

        }
        if (res == null && raiz.getHijoIzquierdo() != null) {
            res = buscarNodo(mat, raiz.getHijoIzquierdo());
        }
        if (res == null && raiz.getHijoDerecho() != null) {
            res = buscarNodo(mat, raiz.getHijoDerecho());
        }

        return res;
//        NodoArbol aux = raiz;
//        while (aux.documento != d) {
//            if (d < aux.documento) {
//                aux = aux.hijoIzquierdo;
//
//            } else {
//                aux = aux.hijoDerecho;
//            }
//            if (aux == null) { //si no encuentra nada retornar nulo 
//                return null;
        // }
//        }
        
        // metodo para borrar un nodo
        
       
        }  
//        public int cambiarEstado(String mat){
//         int estadoN=0;
//         NodoArbol res = null;
//        if (raiz.matricula == mat) {
//            res = raiz;
//
//        }
//        if (res == null && raiz.hijoIzquierdo != null) {
//            res = buscarNodo(mat, raiz.hijoIzquierdo);
//        }
//        if (res == null && raiz.hijoDerecho != null) {
//            res = buscarNodo(mat, raiz.hijoDerecho);
//        }
//        
//        if (res.estado="Deshabilitado"){
//            
//        }
//         return estadoN;
//
//    }
}
