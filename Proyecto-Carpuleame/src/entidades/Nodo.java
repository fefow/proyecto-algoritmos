package entidades;


public class Nodo {

    private Nodo Siguiente;
    private Object Contiene;

    public Nodo() {
    	super();
    }

    public Nodo(Object o) {
        setContiene(o);
    }

    /**
     * @return the Siguiente
     */
    public Nodo getSiguiente() {
        return Siguiente;
    }

    /**
     * @param Siguiente the Siguiente to set
     */
    public void setSiguiente(Nodo Siguiente) {
        this.Siguiente = Siguiente;
    }

    /**
     * @return the Contiene
     */
    public Object getContiene() {
        return Contiene;
    }

    /**
     * @param Contiene the Contiene to set
     */
    public void setContiene(Object Contiene) {
        this.Contiene = Contiene;
    }

}
